package com.nexus6.task11.menu;

import com.nexus6.task11.comm.*;
import com.nexus6.task11.utils.TxtUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class MainMenu {
  private BufferedReader br;
  private Map<String, Executable> exeMenu;

  public MainMenu() {
    br = new BufferedReader(new InputStreamReader(System.in));
    exeMenu = prepareExeMenu();
  }

  private void showTxtMenu() {
    TxtUtils
        .getTxtMenu()
        .get()
        .map(i -> i + "\n")
        .forEach(TxtUtils.appLog::info);
    TxtUtils.appLog.info(TxtUtils.SEL_MENU_ITM);
  }

  public void getChoice() {
    int menuLastItem = exeMenu.size() - 1;
    showTxtMenu();
    try {
      String choice = br.readLine().trim();
      if(choice.matches("[0-" + menuLastItem + "]")) {
        processChoice(choice);
      } else {
        throw new IllegalArgumentException();
      }
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.CONS_INP_ERROR);
    } catch (IllegalArgumentException e) {
      TxtUtils.appLog.warn(TxtUtils.BAD_MENU_ITM);
      getChoice();
    }
  }

  private void processChoice(String c) {
    exeMenu
        .entrySet()
        .stream()
        .filter(i -> i.getKey().equals(c))
        .forEach(i -> i.getValue().execute());
    showPause();
    getChoice();
  }

  private HashMap<String, Executable> prepareExeMenu() {
    HashMap<String, Executable> menu = new LinkedHashMap<>();
    menu.put("0", new ExitAppComm());
    menu.put("1", new ExchangeResourceComm());
    menu.put("2", new FibThreadComm());
    menu.put("3", new CallableTaskComm());
    menu.put("4", new TaskScheduleComm());
    menu.put("5", new IntraThreadComm());
    return menu;
  }

  private void showPause() {
    TxtUtils.appLog.info(TxtUtils.PAUSE_MSG);
    try {
      br.readLine();
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.CONS_INP_ERROR);
    }
  }
}
