package com.nexus6.task11.resexch;

import com.nexus6.task11.utils.TxtUtils;

class Radio {

  synchronized void reportHeight(int h) {
    try {
      notify();
      TxtUtils.appLog.info(Thread.currentThread().getName() + " at " + h + " km\n");
      wait(5);
    } catch (InterruptedException e) {
      TxtUtils.appLog.error(TxtUtils.INTRP_ERR);
    }
  }
}
