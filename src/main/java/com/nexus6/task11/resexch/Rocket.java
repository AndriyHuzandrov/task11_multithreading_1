package com.nexus6.task11.resexch;

import com.nexus6.task11.utils.TxtUtils;

public class Rocket implements Runnable{
  private Thread rocketThread;
  private static Radio radioTransmit;

  public Rocket(String name) {
    radioTransmit = new Radio();
    rocketThread = new Thread(this, name);
    rocketThread.start();
  }

  public Thread getThrd() {
    return rocketThread;
  }

  public void run() {
    for(int i = 0; i < TxtUtils.HEIGHT; i++) {
      radioTransmit.reportHeight(i);
    }
  }
}
