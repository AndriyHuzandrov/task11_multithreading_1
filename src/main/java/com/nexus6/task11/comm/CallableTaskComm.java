package com.nexus6.task11.comm;

import com.nexus6.task11.fib.FibCalc;
import com.nexus6.task11.utils.TxtUtils;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CallableTaskComm implements Executable {

  public void execute() {
    Callable<Long> fibSum1 = () -> new FibCalc(10).getRowSum();
    Callable<Long> fibSum2 = () -> new FibCalc(15).getRowSum();
    ExecutorService fibService = Executors.newFixedThreadPool(2);
    try {
      TxtUtils.appLog.info(TxtUtils.ROW_SUM + fibService.submit(fibSum1).get() + "\n");
      TxtUtils.appLog.info(TxtUtils.ROW_SUM + fibService.submit(fibSum2).get() + "\n");
    } catch (InterruptedException
              | ExecutionException e) {
      TxtUtils.appLog.error(TxtUtils.INTRP_ERR);
    }
    fibService.shutdown();
  }
}
