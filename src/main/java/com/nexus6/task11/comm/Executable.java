package com.nexus6.task11.comm;

public interface Executable {
  void execute();
}
