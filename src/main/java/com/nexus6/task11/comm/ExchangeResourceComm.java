package com.nexus6.task11.comm;

import com.nexus6.task11.resexch.Rocket;
import com.nexus6.task11.utils.TxtUtils;

public class ExchangeResourceComm implements Executable {
  public void execute() {
    Rocket r1 = new Rocket("rocket_1");
    Rocket r2 = new Rocket("rocket_2");
    try {
      r1.getThrd().join();
      r2.getThrd().join();
    } catch (InterruptedException e) {
      TxtUtils.appLog.error(TxtUtils.INTRP_ERR);
    }

  }
}
