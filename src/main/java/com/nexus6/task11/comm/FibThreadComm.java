package com.nexus6.task11.comm;

import com.nexus6.task11.fib.FibCalc;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibThreadComm implements Executable {

  public void execute() {
    Runnable task1 = () -> new FibCalc(5).calcRow();
    Runnable task2 = () -> new FibCalc(10).calcRow();
    ExecutorService fibEx = Executors.newFixedThreadPool(2);
    fibEx.execute(task1);
    fibEx.execute(task2);
    fibEx.shutdown();
  }
}
