package com.nexus6.task11.comm;

public class ExitAppComm implements Executable{

  public void execute() {
    System.exit(0);
  }
}
