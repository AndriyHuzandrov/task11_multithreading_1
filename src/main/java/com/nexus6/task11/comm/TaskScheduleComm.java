package com.nexus6.task11.comm;

import com.nexus6.task11.utils.TxtUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TaskScheduleComm implements Executable {
  private BufferedReader br;

  public TaskScheduleComm() {
    br = new BufferedReader(new InputStreamReader(System.in));
  }

  public void execute() {
    int taskQty = getTaskQty();
    List<Callable<Integer>> tasks = new LinkedList<>();
    Callable<Integer> sleepingTask = () -> {
      Random rnd = new Random();
      int sleepTime = rnd.nextInt(11);
      try {
        TimeUnit.SECONDS.sleep(sleepTime);
      } catch (InterruptedException e) {
        TxtUtils.appLog.error(TxtUtils.INTRP_ERR);
      }
      return sleepTime;
    };
    for(int i = 1; i <= taskQty; i++) {
      tasks.add(sleepingTask);
    }
    ScheduledExecutorService exService = Executors.newScheduledThreadPool(taskQty);
    try {
      exService.invokeAll(tasks)
          .stream()
          .map(this::extractTime)
          .forEach(i -> TxtUtils.appLog.info(TxtUtils.TASK_SLEEP_MSG + i + "\n"));
    } catch (InterruptedException e) {
      TxtUtils.appLog.error(TxtUtils.INTRP_ERR);
    }
    exService.shutdown();
  }

  private int getTaskQty() {
    int qty = 0;
    TxtUtils.appLog.info(TxtUtils.TASK_QTY);
    try {
      String input = br.readLine().trim();
      if(input.matches("\\d+")){
        qty = Integer.parseInt(input);
      } else {
        throw new IllegalArgumentException();
      }
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.CONS_INP_ERROR);
    } catch (IllegalArgumentException e) {
      TxtUtils.appLog.warn(TxtUtils.WRONG_TASK_QTY);
      getTaskQty();
    }
    return qty;
  }

  private Integer extractTime(Future<Integer> f) {
    try {
      return f.get();
    } catch (InterruptedException
        | ExecutionException e) {
      TxtUtils.appLog.error(TxtUtils.INTRP_ERR);
    }
    return -1;
  }
}
