package com.nexus6.task11.comm;

import com.nexus6.task11.utils.TxtUtils;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class IntraThreadComm implements Executable {

  public void execute() {
    try(PipedOutputStream outChan = new PipedOutputStream();
        PipedInputStream inChan = new PipedInputStream()) {
      inChan.connect(outChan);
      Runnable transmitter = () -> {
        for(int i = 1; i <= 10; i++) {
          try {
            outChan.write(1);
          } catch (IOException e) {
            TxtUtils.appLog.error(TxtUtils.IO_ERR);
          }
        }
      };
      Runnable receiver = () -> {
        int data = 0;
        int sum = 0;
        try{
          while((data = inChan.read()) != -1) {
            sum +=data;
          }
        } catch (IOException e) {
          TxtUtils.appLog.error(TxtUtils.IO_ERR);
        }
        TxtUtils.appLog.info(TxtUtils.TRANS_DATA_SUM + sum + "\n");
      };
      ExecutorService exServ = Executors.newFixedThreadPool(2);
      exServ.execute(transmitter);
      exServ.execute(receiver);
      exServ.shutdown();
    } catch (IOException e) {
      TxtUtils.appLog.error(TxtUtils.IO_ERR);
    }
  }
}
