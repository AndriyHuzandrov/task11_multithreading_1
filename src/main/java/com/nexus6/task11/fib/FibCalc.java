package com.nexus6.task11.fib;

import com.nexus6.task11.utils.TxtUtils;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.LongStream;

public class FibCalc {
  private int rowLength;
  private List<Long> result;

  public FibCalc(int l) {
    rowLength = l;
    result = new LinkedList<>();
  }
  public void calcRow() {
    long f1 = 0;
    long f2 = 1;
    long feb;
    for(int i = 0; i <= rowLength; i++) {
      feb = f1 + f2;
      result.add(feb);
      f1 = f2; f2 = feb;
    }
    TxtUtils.appLog.info(Thread.currentThread().getName() +
                          TxtUtils.THRD_REPORT + rowLength + "\n");
  }
  public long getRowSum() {
    calcRow();
    return result
              .stream()
              .reduce(0L, (acc, i) -> acc + i);
  }
}
