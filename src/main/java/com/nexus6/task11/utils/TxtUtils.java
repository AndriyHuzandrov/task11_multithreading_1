package com.nexus6.task11.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TxtUtils {

  public static final Logger appLog = LogManager.getLogger(TxtUtils.class);
  private static List<String> txtMenu;
  public static String CONS_INP_ERROR = "Error reading from console.";
  public static String BAD_MENU_ITM = "Bad menu item";
  public static String SEL_MENU_ITM = "Select menu item: ";
  public static String TASK_QTY = "Enter a number of tasks to be run: ";
  public static String IO_ERR = "Pipe operation error";
  public static String TASK_SLEEP_MSG = "Task slept for ";
  public static String PAUSE_MSG = "Press Enter to continue\n";
  public static String WRONG_TASK_QTY = "Unable to create set task number.\n";
  public static String TRANS_DATA_SUM = "Sum of transmitted data is ";
  public static String STAT_TAB_HEADER = "%30s%10s%n";
  public static String READ_OP = "Read operation";
  public static String TIME = "Time [s]";
  public static String START_B_READ = "Buffered reading from file\n";
  public static String STAT_TAB_ROW = "%30s%10.2f%n";
  public static String BAD_ARR_SIZE = "Bad buffer size. Setting up default value.";
  public static String INTRP_ERR = "Process interrupted\n";
  public static String ROW_SUM = "Sum of Fibonacci row is: ";
  public static String THRD_REPORT = " thread finished. Elements calculated = ";
  public static int HEIGHT = 10;


  static {
    txtMenu = new LinkedList<>();
    txtMenu.add("1. Exchange resource demo.");
    txtMenu.add("2. Calculate Fibonacci row in executor.");
    txtMenu.add("3. Running task with results.");
    txtMenu.add("4. Test task schedule.");
    txtMenu.add("5. Intra thread communication.");
    txtMenu.add("0. Exit");
  }

  private TxtUtils() {
  }

  public static Supplier<Stream<String>> getTxtMenu() {
    return () -> txtMenu.stream();
  }
}